try:
    import Tkinter as tk
    from Tkinter import Frame, Tk, BOTH, Text, Menu, END
except ImportError:
    from tkinter import FRAME, Tk, BOTH, Text, Menu, END
    import tkinter as tk

import tkFileDialog
import os,sys,tkFont,subprocess
from os import listdir
from os.path import isfile, join
import gc #enable default garbage collector

gc.enable()

class ARGUI(Frame):
    startdir = os.getcwd()
    wrkdir = os.getcwd()
    os.chdir(wrkdir)
    fields = ('URL','Batch File')
    defaults = ("No directory selected...","No file selected...")
    err = []
    expdata = []
    
    def __init__(self, parent):
        Frame.__init__(self, parent)
        self.customFont = tkFont.Font(family='Helvetica',size=10)
        self.parent = parent
        self.initUI()
        self.dir_opt = options = {}

        self.file_opt = options = {}
        options['defaultextension'] = '.*'
        options['filetypes'] = [('All files', '*.*')]
        options['initialdir'] = self.wrkdir
        options['initialfile'] = ''
        options['parent'] = self
        options['title'] = 'Select File'

        self.dir_opt = options = {}
        options['initialdir'] = self.wrkdir
        options['mustexist'] = False
        options['parent'] = self
        options['title'] = 'Select Output Directory'

    def getDir(self,direntry):
        currentdir = os.getcwd()
        self.wrkdir = tkFileDialog.askdirectory(**self.dir_opt)
        if self.wrkdir != '':
            os.chdir(self.wrkdir)
            print self.wrkdir
            direntry.delete(0,'end')
            direntry.insert(0,str(self.wrkdir))
        if self.wrkdir == '':
            self.wrkdir = currentdir
            direntry.delete(0,'end')
            direntry.insert(0,str(self.wrkdir))
            

    def startURL(self,urlentry):
        url = urlentry.get()
        print url
        subprocess.Popen(['youtube-dl','-x','--restrict-filenames',url])

    def convert(self,acodec,keep):
        # Get files in directory
        filelist = [f for f in listdir(self.wrkdir) if \
                    isfile(join(self.wrkdir,f))]

        aformat = ['mp3','m4a','wav','opus']

        for afile in filelist:
            if afile.split('.')[-1] == acodec:
                continue
            if afile.split('.')[-1] in aformat:
                newname = afile.split('.')[0]+'.mp3'
                print afile
                print newname
                subprocess.Popen(['ffmpeg','-i',afile,'-acodec', 'mp3',newname])

    def initUI(self):
            
        self.parent.title("Audio Ripper v0.1")
        baseFrame = tk.Frame(self)
        outFrame = tk.LabelFrame(baseFrame,text='Output Directory Settings')
        urlFrame = tk.LabelFrame(baseFrame,text='URL Settings')
        batchFrame = tk.LabelFrame(baseFrame,text='Batch File Settings')
        convertFrame = tk.LabelFrame(baseFrame,text='Audio Conversion Settings')
        self.pack(fill=BOTH,expand=1)

        # Set up output directory stuff
        row0 = tk.Frame(outFrame)
        #lab0 = tk.Label(row0,width=10,text='Out Directory',anchor='w')
        ent0 = tk.Entry(row0)
        b0 = tk.Button(row0,text='Set',font=self.customFont, \
                       command=lambda: self.getDir(ent0))
        row0.pack(side='top',fill='x',padx=5,pady=5)
        #lab0.pack(side='left')
        ent0.pack(side='left',expand='yes',fill='x')
        ent0.insert(0,self.wrkdir)
        b0.pack(side='left',padx=10,pady=5)
        
        # Set up URL entry frame and button.
        row1=tk.Frame(urlFrame)
        #lab1 = tk.Label(row1,width=10,text='URL',anchor='w')
        ent1 = tk.Entry(row1)
        b1 = tk.Button(row1,text='Start',font=self.customFont, \
                       command=lambda: self.startURL(ent1))
        row1.pack(side='top',fill='x',padx=5,pady=5)
        #lab1.pack(side='left')
        ent1.pack(side='left',expand='yes',fill='x')
        ent1.insert(0,'No URL Selected')
        b1.pack(side='left',padx=10,pady=5)
               
        # Set up batch file entry frame and button.
    
        # Set up audio conversion frame
        row3 = tk.Frame(convertFrame)
        b3 = tk.Button(row3,text="Convert",font=self.customFont, \
                       command=lambda: self.convert('.mp3',False))
        row3.pack(side='top',fill='x',padx=5,pady=5)
        b3.pack(side='top',padx=5,pady=5)

        #entries = self.makeform(entryFrame,self.fields,self.defaults)
        
        ## Set up drop down menus ##
        # Set up work directory, file options menu
        
        baseFrame.pack(side='top',expand=1,fill=tk.BOTH,padx=5,pady=5)
        outFrame.pack(side='top',expand=1,fill=tk.BOTH,padx=5,pady=5)
        urlFrame.pack(side='top',expand=1,fill=tk.BOTH,padx=5,pady=5)
        convertFrame.pack(side='top',expand=1,fill=tk.BOTH,padx=5,pady=5)


def center(toplevel):
    toplevel.update_idletasks()
    w = toplevel.winfo_screenwidth()
    h = toplevel.winfo_screenheight()
    size = tuple(int(_) for _ in toplevel.geometry().split('+')[0].split('x'))
    x = w/2 - size[0]/2
    y = h/2 - size[0]/2
    toplevel.geometry("%dx%d+%d+%d" %(size+(x,y)))

def main():
    root = Tk()
    win = ARGUI(root)
    root.geometry("500x300")
    root.resizable(width=tk.FALSE, height=tk.FALSE)
    root.eval('tk::PlaceWindow %s center' % root.winfo_pathname(root.winfo_id()))
    root.mainloop()


if __name__ == '__main__':
    main()
